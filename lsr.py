from __future__ import print_function # to avoid issues between Python 2 and 3 printing

import sys
import numpy as np
import math
import utilities
from scipy import stats
from skimage import data, io, color, transform, exposure
from pprint import pprint
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

'''
    This script calculates least squares regression for values provided in .csv format (multiple of 20 lines long).
    
    It uses linear, polynomial (of fixed order 3 for now) and sine functions to determine the
    best fitting line and prints the total error (between model and actual values).
    
    To run:
        > python lsr.py [csv file] [--plot]
    
        where --plot being passed will show a visual representation of line segments and model functions

    - Aidan Sweeney (www.gitlab.com/as17953)
d
'''

# Calculate parameters for a linear function
def linearCalculate(arrayOfXs, arrayOfYs):

    # Create columns for matrix X
    firstColumn = np.ones(20)
    secondColumn = np.array(arrayOfXs)

    # Combine columns to make X and define the transpose of X (XT)
    X = np.column_stack((firstColumn, secondColumn))
    XT = X.transpose()

    # Create matrix Y
    Y = np.array(arrayOfYs).reshape(20, 1)

    # Calculate parameter matrix A from the least squares formula:
    # ** A = ((X^T x X)^-1) x X^T x Y
    A = ((np.linalg.inv(XT.dot(X))).dot(XT)).dot(Y)

    # Extract the coefficients from the parameter matrix
    a = A[0][0]
    b = A[1][0]

    # Calculate a lambda function for the model parameters
    f = lambda x: (a + (np.float64(x) * b))

    # Display a graph with the original points and the modelled function
    #plt.scatter(arrayOfXs, arrayOfYs)
    #plt.plot(arrayOfXs, f(arrayOfXs))
    #plt.show()

    # Return the coefficients
    return a, b

# Calculate parameters for a order 3 polynomial function
def polynomialCalculate(arrayOfXs, arrayOfYs):

    # Create columns for matrix X
    firstColumn = np.ones(20)
    secondColumn = np.array(arrayOfXs)
    thirdColumn = secondColumn * secondColumn
    fourthColumn = thirdColumn * secondColumn

    # Combine columns to make X and define the transpose of X (XT)
    X = np.column_stack((firstColumn, secondColumn, thirdColumn, fourthColumn))
    XT = X.transpose()

    # Create matrix Y
    Y = np.array(arrayOfYs).reshape(20, 1)

    # Calculate parameter matrix A from the least squares formula:
    # ** A = ((X^T x X)^-1) x X^T x Y
    A = ((np.linalg.inv(XT.dot(X))).dot(XT)).dot(Y)

    # Extract the coefficients from the parameter matrix
    a = A[0][0]
    b = A[1][0]
    c = A[2][0]
    d = A[3][0]

    # Calculate a lambda function for the model parameters
    f = lambda x: (a + (np.float64(x) * b) + ((np.float64(x) ** 2) * c) + ((np.float64(x) ** 3) * d))

    # Display a graph with the original points and the modelled function
    #plt.scatter(arrayOfXs, arrayOfYs)
    #plt.plot(arrayOfXs, f(arrayOfXs))
    #plt.show()

    # Return the coefficients
    return a, b, c, d

# Calculate parameters for a sine function
def unknownCalculate(arrayOfXs, arrayOfYs):

    # Create columns for matrix X
    firstColumn = np.ones(20)
    secondColumn = np.array(np.sin(arrayOfXs))


    # Combine columns to make matrix X and define X transpose, XT
    X = np.column_stack((firstColumn, secondColumn))
    XT = X.transpose()

    # Create matrix Y
    Y = np.array(arrayOfYs).reshape(20, 1)

    # Calculate parameter matrix A from the least squares formula:
    # ** A = ((X^T x X)^-1) x X^T x Y
    A = ((np.linalg.inv(XT.dot(X))).dot(XT)).dot(Y)

    # Extract the function coefficients from the parameter matrix
    a = A[0][0]
    b = A[1][0]

    # Calculate a lambda function for the model parameters
    f = lambda x: (a + (b * np.sin(np.float64(x))))

    # Display a graph with the original points and the modelled function
    #plt.scatter(arrayOfXs, arrayOfYs)
    #plt.plot(arrayOfXs, f(arrayOfXs))
    #plt.show()

    # Return coefficients
    return a, b

# Calculate error for a linear function model
def linearErrorCalculate(arrayOfXs, arrayOfYs, a, b):

    # Keep track of total error
    totalError = 0

    # Iterate over every point
    for i in range(20):

        # Calculate the y value from the model parameters
        y = (a + (b * arrayOfXs[i]))

        # Calculate the error between the model y value and the actual y value
        error = (y - arrayOfYs[i]) ** 2

        # Add this error value to the line segment's total error
        totalError += error

    # Return the total error
    return totalError

# Calculate error for a polynomial function model
def polynomialErrorCalculate(arrayOfXs, arrayOfYs, a, b, c, d):

    # Keep track of total error
    totalError = 0

    # Iterate over every point
    for i in range(20):

        # Calculate the y value from the model parameters
        y = (a + (arrayOfXs[i] * b) + ((arrayOfXs[i] ** 2) * c) + ((arrayOfXs[i] ** 3) * d))

        # Calculate the error between the model y value and the actual y value
        error = (y - arrayOfYs[i]) ** 2

        # Add this error value to the line segment's total error
        totalError += error

    # Return the total error
    return totalError

# Calculate error for an unknown function model
def unknownErrorCalculate(arrayOfXs, arrayOfYs, a, b):

    # Keep track of total error
    totalError = 0

    # Iterate over every point
    for i in range(20):
        # Calculate the y value from the model parameters
        y = (a + (b * np.sin(np.float64(arrayOfXs[i]))))

        # Calculate the error between the model y value and the actual y value
        error = (y - arrayOfYs[i]) ** 2

        # Add this error value to the line segment's total error
        totalError += error

    # Return the total error
    return totalError

# Main function determines what to do whens script is run
def main(args):

    # Get the file name
    fileName = args[0]

    # Read in all x and y values from the file
    # Read in all x and y values from the file
    allValues = utilities.load_points_from_file(fileName)

    # Split all points into x and y values
    allXValues = allValues[0]
    allYValues = allValues[1]

    # Check if --plot was passed
    if ((len(args) > 1)):

        if (args[1] == '--plot'):

            # Set a flag to record that the graph should be displayed
            plotPassed = True
        else:
            raise RuntimeError("To plot the graph, pass '--plot' when calling the script")

    # If --plot isn't passed
    else:

        # Set flag to signal not to visualise
        plotPassed = False

    # Create a variable for the number of line segments
    noOfSegments = (int(len(allXValues)/20))

    # Declare an array to record what model is best fit for each line segment
    # E.g. if chosenModel[1] = 0 the second line (index 1) segment had minimal error with the linear model (index 0)
    # if chosenModel[0] = 2 the first line (index 0) segment had a minimal error with the unknown model (index 2)
    chosenModel = [-1] * noOfSegments

    # Declare two arrays for the line segments' points to be stored
    segmentXValues = [[0] * 20] * noOfSegments
    segmentYValues = [[0] * 20] * noOfSegments

    # Create a counter for overall error for all line segments
    totalError = 0

    # Iterate over all groups of 20 points to calculate the best fit model
    for i in range(noOfSegments):

        # Declare an array to track error values from different function estimations
        errorValues = [0] * 3

        # Iterate over all points in the line segment
        for j in range(20):

            # Add the points to the segment arrays
            segmentXValues[i][j] = allXValues[(i*20) + j]
            segmentYValues[i][j] = allYValues[(i*20) + j]

        # Calculate the model parameters for linear, polynomial and unknown functions
        linA, linB = linearCalculate(segmentXValues[i], segmentYValues[i])
        polyA, polyB, polyC, polyD = polynomialCalculate(segmentXValues[i], segmentYValues[i])
        unknownA, unknownB = unknownCalculate(segmentXValues[i], segmentYValues[i])

        # Add the error values to the error array accordingly
        errorValues[0] = linearErrorCalculate(segmentXValues[i], segmentYValues[i], linA, linB)
        errorValues[1] = polynomialErrorCalculate(segmentXValues[i], segmentYValues[i], polyA, polyB, polyC, polyD)
        errorValues[2] = unknownErrorCalculate(segmentXValues[i], segmentYValues[i], unknownA, unknownB)

        # Decide to choose linear model over polynomial if polynomial error isn't too far off linear error
        if (errorValues[1] <= 1.1 * errorValues[0] and errorValues[1] >= 0.9 * errorValues[0]):

            # Set the error values to be equal, so that linear will be chosen as the model
            # NOTE: No need to change how chosenModel is chosen, since if both indices 0 and 1 have the same value,
            #       index 0 will be chosen by the way [array].index works
            errorValues[1] = errorValues[0]


        # Set the chosen model to the model that gives the minimum error value
        chosenModel[i] = errorValues.index(min(errorValues))

        # Add this error value to the total error for all line segments
        totalError += min(errorValues)

        # If the visualisation was called, create the scatter to be shown
        if (plotPassed):

            # For linear functions
            if (chosenModel[i] == 0):

                # Determine the function to plot
                f = lambda x: (linA + (linB * np.float64(x)))

            # For polynomial functions
            elif (chosenModel[i] == 1):

                # Determine the function to plot
                f = lambda x: (polyA + (np.float64(x) * polyB) + ((np.float64(x) ** 2) * polyC) + ((np.float64(x) ** 3) * polyD))

            # For unknown functions
            else:

                # Determine the function to plot
                f = lambda x: (unknownA + (unknownB * np.sin(np.float64(x))))

            # Add the line segment points and model function to the list of things to be displayed to visualiser
            plt.scatter(segmentXValues[i], segmentYValues[i])
            plt.plot(segmentXValues[i], f(segmentXValues[i]))



    if (plotPassed):

        plt.show()

    # Print the total error
    print(totalError, "\n")

# Calls the main function, passing necessary arguments
if __name__ == '__main__':

    # Arguments come through in the format:
    # python [script path] [file path] [--plot]
    main(sys.argv[1:])


