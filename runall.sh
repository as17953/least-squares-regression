#!/bin/bash
scriptPath=/home/aidan/Documents/repos/least-squares-regression/lsr.py
testDirectory=/home/aidan/Documents/repos/least-squares-regression/train
echo $scriptPath $testDirectory

for i in $(ls $testDirectory)
do
	echo For $i:
	python $scriptPath $testDirectory/$i
done
